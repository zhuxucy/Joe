<?php

/**
 * 友链
 * 
 * @package custom 
 * 
 **/

?>

<!DOCTYPE html>
<html lang="zh-CN">

<head>
  <?php $this->need('public/include.php'); ?>
  <?php if ($this->options->JPrismTheme) : ?>
    <link href="<?php $this->options->JPrismTheme() ?>" rel="stylesheet">
  <?php else : ?>
    <link href="<?php _getAssets('assets/lib/prism/prism.min.css'); ?>" rel="stylesheet">
  <?php endif; ?>
  <script src="<?php _getAssets('assets/lib/clipboard@2.0.11/clipboard.min.js'); ?>"></script>
  <script src="<?php _getAssets('assets/lib/prism/prism.min.js'); ?>"></script>
  <script src="<?php _getAssets('assets/js/joe.post_page.min.js'); ?>"></script>
</head>

<body>
  <div id="Joe">
    <?php $this->need('public/header.php'); ?>
    <div class="joe_container">
    <?php $this->need('public/aside-the.php'); ?>
      <div class="joe_main">
        <div class="joe_detail" data-cid="<?php echo $this->cid ?>">
          <?php $this->need('public/batten.php'); ?>
          <?php $this->need('public/article.php'); ?>
          <?php
          $friends = [];
          $friends_color = [
            '#858585',
          ];
          $friends_text = $this->options->JFriends;
          if ($friends_text) {
            $friends_arr = explode("\r\n", $friends_text);
            if (count($friends_arr) > 0) {
              for ($i = 0; $i < count($friends_arr); $i++) {
                $name = explode("||", $friends_arr[$i])[0];
                $url = explode("||", $friends_arr[$i])[1];
                $avatar = explode("||", $friends_arr[$i])[2];
                $desc = explode("||", $friends_arr[$i])[3];
                $friends[] = array("name" => trim($name), "url" => trim($url), "avatar" => trim($avatar), "desc" => trim($desc));
              };
            }
          }
          ?>
          <?php if (sizeof($friends) > 0) : ?>
            <ul class="joe_detail__friends">
              <?php foreach ($friends as $item) : ?>
                <li class="joe_detail__friends-item">
                  <a class="contain" href="<?php echo $item['url']; ?>" target="_blank" rel="noopener noreferrer" style="background: <?php echo $friends_color[mt_rand(0, count($friends_color) - 1)] ?>">
                    <span class="title"><?php echo $item['name']; ?></span>
                    <div class="content">
                      <div class="desc"><?php echo $item['desc']; ?></div>
                      <img width="40" height="40" class="avatar lazyload" src="<?php _getAvatarLazyload(); ?>" data-src="<?php echo $item['avatar']; ?>" alt="<?php echo $item['name']; ?>" />
                    </div>
                  </a>
                </li>
              <?php endforeach; ?>
            </ul>
          <?php endif; ?>
          <?php $this->need('public/handle.php'); ?>
        </div>
        <?php $this->need('public/comment.php'); ?>
      </div>
      <?php $this->need('public/aside.php'); ?>
    </div>
    <?php $this->need('public/footer.php'); ?>
  </div>
</body>

</html>
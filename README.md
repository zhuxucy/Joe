## Joe

> 一款基于 Typecho 博客的双栏极致优化主题

- QQ 交流群：151407331
- Git 仓库：https://gitee.com/zhuxucy/joe
- 主题宗旨：简洁、超强、开源、精华

#### 主题简介

1、整包仅5.20Mb，却实现超强功能，极其迅速的响应
2、全站变量名、类名统一规范，重在方便更多人参与二开与拓展  
3、主题开箱即用，没有任何复杂的操作，无需像其他主题去特意创建个分类等。
4、主题首发Typecho独家Joe编辑器
5、主题响应式布局，不依赖任何响应式框架，采用 Joe 独家响应式
6、主题在一切可能暴露的接口上，屏蔽sql注入、xss攻击风险，提供安全保障
7、主题SEO极致优化，Lighthouse SEO跑分彪满100分
8、主题色彩全局公用、小白轻松直接修改整站自定义主题色
9、主题内置代码高亮、无需借助任何插件、支持200种语言
10、主题内置sitemap、百度推送、友联、回复可见等，无需依赖任何插件

#### 主题目录介绍

├── assets 主题静态资源

├── core 主题核心文件夹

├── library 主题内集成第三方库

├── public 共用的一些模块文件

├── 404.php 404 页面

├── archive.php 搜索页面

├── friends.php 友情链接页面

├── functions.php 主题的外观、功能设置

├── index.php 博客首页页面

├── leaving.php 留言板页面

├── page.php 独立页面

├── post.php 文章页面

└── screenshot.php 主题略缩图片

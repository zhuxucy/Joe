<?php
if (!defined('__TYPECHO_ROOT_DIR__')) exit;

/* Joe核心文件 */
require_once("core/core.php");

function themeConfig($form)
{
  $_db = Typecho_Db::get();
  $_prefix = $_db->getPrefix();
  try {
    if (!array_key_exists('views', $_db->fetchRow($_db->select()->from('table.contents')->page(1, 1)))) {
      $_db->query('ALTER TABLE `' . $_prefix . 'contents` ADD `views` INT DEFAULT 0;');
    }
    if (!array_key_exists('agree', $_db->fetchRow($_db->select()->from('table.contents')->page(1, 1)))) {
      $_db->query('ALTER TABLE `' . $_prefix . 'contents` ADD `agree` INT DEFAULT 0;');
    }
  } catch (Exception $e) {
  }
?>
  <link rel="stylesheet" href="<?php _getAssets('assets/typecho/config/css/joe.config.min.css') ?>">
  <script src="<?php _getAssets('assets/typecho/config/js/joe.config.min.js') ?>"></script>
  <div class="joe_config">
    <div>
      <div class="joe_config__aside">
        <div class="logo">Joe <?php echo _getVersion() ?></div>
        <ul class="tabs">
          <!--<li class="item" data-current="joe_notice">最新公告</li>-->
          <li class="item" data-current="joe_global">全局设置</li>
          <li class="item" data-current="joe_image">图片设置</li>
          <li class="item" data-current="joe_post">文章设置</li>
          <li class="item" data-current="joe_index">首页设置</li>
          <li class="item" data-current="joe_aside">侧栏设置</li>
          <li class="item" data-current="joe_other">其他设置</li>
        </ul>
        <?php require_once('core/backup.php'); ?>
      </div>
    </div>
    <div class="joe_config__notice">请求数据中...</div>
  <?php
  $JFavicon = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JFavicon',
    NULL,
    'data:image/x-icon;base64,AAABAAEAICAAAAEAIACoEAAAFgAAACgAAAAgAAAAQAAAAAEAIAAAAAAAABAAABILAAASCwAAAAAAAAAAAAAAAAAA7aoAAO2qAAztqgBo7aoAye2qAPrtqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAPvtqgDK7aoAau2qAA/tqgAAAAAAAO2qAADtqgAb7aoAn+2qAPftqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD47aoAp+2qABvtqgAA7aoAC+2qAKDtqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoAp+2qAA/tqgBh7aoA9e2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD37aoAa+2qAMLtqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgDJ7aoA9e2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAPrtqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2pAP/tqQD/7akA/+2pAP/tqQD/7akA/+2qAP/tqgD/7akA/+2pAP/tqQD/7akA/+2pAP/tqQD/7akA/+2pAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqwT/8Lku//PHV//0yl//9Mpf//TKYP/zx1f/7q8P/+2pAP/wuSz/9Mph//TKYP/0ymD/9Mpf//TKX//zx1j/8Low/+2rBP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoB//TNav/99+j///79/////v////7////////9+v/0zWn/7agA//G7M//99uT//////////v////7////+///+/f/++Or/9c9v/+2rAv/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2pAP/vtSD//fPc//////////////////////////////////zx1f/vtSH/7akA//bUf//////////////////////////////////99eH/8LYm/+2pAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7akA//G+PP/++/T//////////////////////////////////vrx//TMZ//tqAD/77Mc//vuzf////////////////////////////789v/xvz//7akA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqQD/8b8///789v////////////////////////79//rlsv/xvj3/7q8Q/+2qAP/tqQD/88hb//789////////////////////////vz2//G/P//tqQD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2pAP/xvz///vz2///////////////////////657j/7rEW/+2pAP/tqgD/7aoA/+2qAP/urQv/+eOs///////////////////////+/Pb/8b8//+2pAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7akA//G/P//+/Pb//////////////////vv0//LCSP/tqQD/7aoA/+2qAP/tqgD/7aoA/+2pAP/ywUb//vv0//////////////////789v/xvz//7akA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqQD/8b8///789v/////////////////99eH/77Qf/+2pAP/tqgD/7aoA/+2qAP/tqgD/7akA/++0H//99eH//////////////////vz2//G/P//tqQD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2pAP/xvz///vz2//////////////////314f/vtB//7akA/+2qAP/tqgD/7aoA/+2qAP/tqQD/77Qe//314f/////////////////+/Pb/8b8//+2pAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7akA//G/P//+/Pb//////////////////vrx//G/QP/tqQD/7aoA/+2qAP/tqgD/7aoA/+2pAP/xvz///vrx//////////////////789v/xvz//7akA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqQD/8b8///789v//////////////////////+eOq/+6uDv/tqQD/7aoA/+2qAP/tqQD/7q4O//niqf///////////////////////vz2//G/P//tqQD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2pAP/wujH//vns/////////////////////////vv/+N+g//C5Lf/urQr/7q0K//C5Lf/435////77///////////////////////++vD/8bw2/+2pAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+6uC//546v///////////////////////////////7//ffn//vrw//768P//ffn/////v////////////////////////////rmtP/urw7/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7akA/++0Hf/4357//vz3///////////////////////////////////////////////////////////////////9+P/54aX/77Yj/+2pAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7akA/+6tCv/0y2T//fTe///////////////////////////////////////////////////////99eH/9M1p/+6tC//tqQD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2pAP/wujD/+uWy///++////////////////////////////////////vz/+ua1//G7NP/tqQD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2pAP/urxD/9dJ3//346f///////////////////////vjr//bTfP/urxL/7akA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgL/8r9A//vqwf///v3////+//vsxv/ywET/7aoD/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqQD/77IY//TMZ//0zWn/77Mb/+2pAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA9O2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7akA/+2pAP/tqQD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAPntqgC97aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoAyO2qAFrtqgDz7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAPbtqgBo7aoACe2qAJntqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoAn+2qAAvtqgAA7aoAGe2qAJntqgD07aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA9u2qAKHtqgAb7aoAAAAAAADtqgAA7aoACu2qAFntqgC+7aoA9e2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA9u2qAMPtqgBh7aoADO2qAAAAAAAAwAAAA4AAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAcAAAAM=',
    '网站 Favicon 设置',
    '介绍：用于设置网站 Favicon，一个好的 Favicon 可以给用户一种很专业的观感 <br />
         格式：图片 URL地址 或 Base64 地址 <br />
         其他：免费转换 Favicon 网站 <a target="_blank" href="//tool.lu/favicon">tool.lu/favicon</a>'
  );
  $JFavicon->setAttribute('class', 'joe_content joe_image');
  $form->addInput($JFavicon);
  /* --------------------------------------- */
  $JLogo = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JLogo',
    NULL,
    'data:image/x-icon;base64,AAABAAEAICAAAAEAIACoEAAAFgAAACgAAAAgAAAAQAAAAAEAIAAAAAAAABAAABILAAASCwAAAAAAAAAAAAAAAAAA7aoAAO2qAAztqgBo7aoAye2qAPrtqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAPvtqgDK7aoAau2qAA/tqgAAAAAAAO2qAADtqgAb7aoAn+2qAPftqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD47aoAp+2qABvtqgAA7aoAC+2qAKDtqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoAp+2qAA/tqgBh7aoA9e2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD37aoAa+2qAMLtqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgDJ7aoA9e2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAPrtqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2pAP/tqQD/7akA/+2pAP/tqQD/7akA/+2qAP/tqgD/7akA/+2pAP/tqQD/7akA/+2pAP/tqQD/7akA/+2pAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqwT/8Lku//PHV//0yl//9Mpf//TKYP/zx1f/7q8P/+2pAP/wuSz/9Mph//TKYP/0ymD/9Mpf//TKX//zx1j/8Low/+2rBP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoB//TNav/99+j///79/////v////7////////9+v/0zWn/7agA//G7M//99uT//////////v////7////+///+/f/++Or/9c9v/+2rAv/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2pAP/vtSD//fPc//////////////////////////////////zx1f/vtSH/7akA//bUf//////////////////////////////////99eH/8LYm/+2pAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7akA//G+PP/++/T//////////////////////////////////vrx//TMZ//tqAD/77Mc//vuzf////////////////////////////789v/xvz//7akA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqQD/8b8///789v////////////////////////79//rlsv/xvj3/7q8Q/+2qAP/tqQD/88hb//789////////////////////////vz2//G/P//tqQD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2pAP/xvz///vz2///////////////////////657j/7rEW/+2pAP/tqgD/7aoA/+2qAP/urQv/+eOs///////////////////////+/Pb/8b8//+2pAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7akA//G/P//+/Pb//////////////////vv0//LCSP/tqQD/7aoA/+2qAP/tqgD/7aoA/+2pAP/ywUb//vv0//////////////////789v/xvz//7akA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqQD/8b8///789v/////////////////99eH/77Qf/+2pAP/tqgD/7aoA/+2qAP/tqgD/7akA/++0H//99eH//////////////////vz2//G/P//tqQD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2pAP/xvz///vz2//////////////////314f/vtB//7akA/+2qAP/tqgD/7aoA/+2qAP/tqQD/77Qe//314f/////////////////+/Pb/8b8//+2pAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7akA//G/P//+/Pb//////////////////vrx//G/QP/tqQD/7aoA/+2qAP/tqgD/7aoA/+2pAP/xvz///vrx//////////////////789v/xvz//7akA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqQD/8b8///789v//////////////////////+eOq/+6uDv/tqQD/7aoA/+2qAP/tqQD/7q4O//niqf///////////////////////vz2//G/P//tqQD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2pAP/wujH//vns/////////////////////////vv/+N+g//C5Lf/urQr/7q0K//C5Lf/435////77///////////////////////++vD/8bw2/+2pAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+6uC//546v///////////////////////////////7//ffn//vrw//768P//ffn/////v////////////////////////////rmtP/urw7/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7akA/++0Hf/4357//vz3///////////////////////////////////////////////////////////////////9+P/54aX/77Yj/+2pAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7akA/+6tCv/0y2T//fTe///////////////////////////////////////////////////////99eH/9M1p/+6tC//tqQD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2pAP/wujD/+uWy///++////////////////////////////////////vz/+ua1//G7NP/tqQD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2pAP/urxD/9dJ3//346f///////////////////////vjr//bTfP/urxL/7akA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgL/8r9A//vqwf///v3////+//vsxv/ywET/7aoD/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqQD/77IY//TMZ//0zWn/77Mb/+2pAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA9O2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7akA/+2pAP/tqQD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAPntqgC97aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoAyO2qAFrtqgDz7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAPbtqgBo7aoACe2qAJntqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoAn+2qAAvtqgAA7aoAGe2qAJntqgD07aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA9u2qAKHtqgAb7aoAAAAAAADtqgAA7aoACu2qAFntqgC+7aoA9e2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA/+2qAP/tqgD/7aoA9u2qAMPtqgBh7aoADO2qAAAAAAAAwAAAA4AAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAcAAAAM=',
    '网站 Logo 设置',
    '介绍：用于设置网站 Logo，一个好的 Logo 能为网站带来有效的流量 <br />
         格式：图片 URL地址 或 Base64 地址 <br />
         其他：免费制作 logo 网站 <a target="_blank" href="//www.uugai.com">www.uugai.com</a>'
  );
  $JLogo->setAttribute('class', 'joe_content joe_image');
  $form->addInput($JLogo);
  /* --------------------------------------- */
  $JAssetsURL = new Typecho_Widget_Helper_Form_Element_Text(
    'JAssetsURL',
    NULL,
    NULL,
    '自定义静态资源CDN地址（非必填）',
    '介绍：自定义静态资源CDN地址，不填则走本地资源 <br />
     教程：<br />
     1. 将整个assets目录上传至你的CDN <br />
     2. 填写静态资源地址访问的前缀 <br />'
  );
  $JAssetsURL->setAttribute('class', 'joe_content joe_global');
  $form->addInput($JAssetsURL);
  /* --------------------------------------- */
  $JCommentStatus = new Typecho_Widget_Helper_Form_Element_Select(
    'JCommentStatus',
    array(
      'on' => '开启（默认）',
      'off' => '关闭'
    ),
    '3',
    '开启或关闭全站评论',
    '介绍：用于一键开启关闭所有页面的评论 <br>
         注意：此处的权重优先级最高 <br>
         若关闭此项而文章内开启评论，评论依旧为关闭状态'
  );
  $JCommentStatus->setAttribute('class', 'joe_content joe_global');
  $form->addInput($JCommentStatus->multiMode());
  /* --------------------------------------- */
  $JNavMaxNum = new Typecho_Widget_Helper_Form_Element_Select(
    'JNavMaxNum',
    array(
      '3' => '3个（默认）',
      '4' => '4个',
      '5' => '5个',
      '6' => '6个',
      '7' => '7个',
    ),
    '3',
    '选择导航栏最大显示的个数',
    '介绍：用于设置最大多少个后，以更多下拉框显示'
  );
  $JNavMaxNum->setAttribute('class', 'joe_content joe_global');
  $form->addInput($JNavMaxNum->multiMode());
  /* --------------------------------------- */
  $JCustomNavs = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JCustomNavs',
    NULL,
    NULL,
    '导航栏自定义链接（非必填）',
    '介绍：用于自定义导航栏链接 <br />
         格式：跳转文字 || 跳转链接（中间使用两个竖杠分隔）<br />
         其他：一行一个，一行代表一个超链接 <br />
         例如：<br />
            玖久的小窝 || https://blog.zhuxu.xyz/
         '
  );
  $JCustomNavs->setAttribute('class', 'joe_content joe_global');
  $form->addInput($JCustomNavs);
  /* --------------------------------------- */
  $JList_Animate = new Typecho_Widget_Helper_Form_Element_Select(
    'JList_Animate',
    array(
      'off' => '关闭（默认）',
      'bounce' => 'bounce',
      'flash' => 'flash',
      'pulse' => 'pulse',
      'rubberBand' => 'rubberBand',
      'headShake' => 'headShake',
      'swing' => 'swing',
      'tada' => 'tada',
      'wobble' => 'wobble',
      'jello' => 'jello',
      'heartBeat' => 'heartBeat',
      'bounceIn' => 'bounceIn',
      'bounceInDown' => 'bounceInDown',
      'bounceInLeft' => 'bounceInLeft',
      'bounceInRight' => 'bounceInRight',
      'bounceInUp' => 'bounceInUp',
      'bounceOut' => 'bounceOut',
      'bounceOutDown' => 'bounceOutDown',
      'bounceOutLeft' => 'bounceOutLeft',
      'bounceOutRight' => 'bounceOutRight',
      'bounceOutUp' => 'bounceOutUp',
      'fadeIn' => 'fadeIn',
      'fadeInDown' => 'fadeInDown',
      'fadeInDownBig' => 'fadeInDownBig',
      'fadeInLeft' => 'fadeInLeft',
      'fadeInLeftBig' => 'fadeInLeftBig',
      'fadeInRight' => 'fadeInRight',
      'fadeInRightBig' => 'fadeInRightBig',
      'fadeInUp' => 'fadeInUp',
      'fadeInUpBig' => 'fadeInUpBig',
      'fadeOut' => 'fadeOut',
      'fadeOutDown' => 'fadeOutDown',
      'fadeOutDownBig' => 'fadeOutDownBig',
      'fadeOutLeft' => 'fadeOutLeft',
      'fadeOutLeftBig' => 'fadeOutLeftBig',
      'fadeOutRight' => 'fadeOutRight',
      'fadeOutRightBig' => 'fadeOutRightBig',
      'fadeOutUp' => 'fadeOutUp',
      'fadeOutUpBig' => 'fadeOutUpBig',
      'flip' => 'flip',
      'flipInX' => 'flipInX',
      'flipInY' => 'flipInY',
      'flipOutX' => 'flipOutX',
      'flipOutY' => 'flipOutY',
      'rotateIn' => 'rotateIn',
      'rotateInDownLeft' => 'rotateInDownLeft',
      'rotateInDownRight' => 'rotateInDownRight',
      'rotateInUpLeft' => 'rotateInUpLeft',
      'rotateInUpRight' => 'rotateInUpRight',
      'rotateOut' => 'rotateOut',
      'rotateOutDownLeft' => 'rotateOutDownLeft',
      'rotateOutDownRight' => 'rotateOutDownRight',
      'rotateOutUpLeft' => 'rotateOutUpLeft',
      'rotateOutUpRight' => 'rotateOutUpRight',
      'hinge' => 'hinge',
      'jackInTheBox' => 'jackInTheBox',
      'rollIn' => 'rollIn',
      'rollOut' => 'rollOut',
      'zoomIn' => 'zoomIn',
      'zoomInDown' => 'zoomInDown',
      'zoomInLeft' => 'zoomInLeft',
      'zoomInRight' => 'zoomInRight',
      'zoomInUp' => 'zoomInUp',
      'zoomOut' => 'zoomOut',
      'zoomOutDown' => 'zoomOutDown',
      'zoomOutLeft' => 'zoomOutLeft',
      'zoomOutRight' => 'zoomOutRight',
      'zoomOutUp' => 'zoomOutUp',
      'slideInDown' => 'slideInDown',
      'slideInLeft' => 'slideInLeft',
      'slideInRight' => 'slideInRight',
      'slideInUp' => 'slideInUp',
      'slideOutDown' => 'slideOutDown',
      'slideOutLeft' => 'slideOutLeft',
      'slideOutRight' => 'slideOutRight',
      'slideOutUp' => 'slideOutUp',
    ),
    'off',
    '选择一款炫酷的列表动画',
    '介绍：开启后，列表将会显示所选择的炫酷动画'
  );
  $JList_Animate->setAttribute('class', 'joe_content joe_global');
  $form->addInput($JList_Animate->multiMode());
  /* --------------------------------------- */
  $JFooter_Left = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JFooter_Left',
    NULL,
    '2023 © Reach - 网站名称',
    '自定义底部栏上方内容（非必填）',
    '介绍：用于修改全站底部上方内容（wap端上方） <br>
         例如：2023 © Reach - 网站名称'
  );
  $JFooter_Left->setAttribute('class', 'joe_content joe_global');
  $form->addInput($JFooter_Left);
  /* --------------------------------------- */
  $JFooter_Right = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JFooter_Right',
    NULL,
    '',
    '自定义底部栏下方内容（非必填）',
    '介绍：用于修改全站底部右侧内容（wap端下方） <br>
         例如：&lt;a href="/"&gt;首页&lt;/a&gt; &lt;a href="/"&gt;关于&lt;/a&gt;'
  );
  $JFooter_Right->setAttribute('class', 'joe_content joe_global');
  $form->addInput($JFooter_Right);
  /* --------------------------------------- */
  $JDocumentTitle = new Typecho_Widget_Helper_Form_Element_Text(
    'JDocumentTitle',
    NULL,
    NULL,
    '网页被隐藏时显示的标题',
    '介绍：在PC端切换网页标签时，网站标题显示的内容。如果不填写，则默认不开启 <br />
         注意：严禁加单引号或双引号！！！否则会导致网站出错！！'
  );
  $JDocumentTitle->setAttribute('class', 'joe_content joe_global');
  $form->addInput($JDocumentTitle);
  /* --------------------------------------- */
  $JCursorEffects = new Typecho_Widget_Helper_Form_Element_Select(
    'JCursorEffects',
    array(
      'off' => '关闭（默认）',
      'cursor1.js' => '效果1',
      'cursor2.js' => '效果2',
      'cursor3.js' => '效果3',
      'cursor4.js' => '效果4',
      'cursor5.js' => '效果5',
      'cursor6.js' => '效果6',
      'cursor7.js' => '效果7',
      'cursor8.js' => '效果8',
      'cursor9.js' => '效果9',
      'cursor10.js' => '效果10',
      'cursor11.js' => '效果11',
    ),
    'off',
    '选择鼠标特效',
    '介绍：用于开启炫酷的鼠标特效'
  );
  $JCursorEffects->setAttribute('class', 'joe_content joe_global');
  $form->addInput($JCursorEffects->multiMode());
  /* --------------------------------------- */
  $JCustomCSS = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JCustomCSS',
    NULL,
    NULL,
    '自定义CSS（非必填）',
    '介绍：请填写自定义CSS内容，填写时无需填写style标签。<br />
         其他：如果想修改主题色、卡片透明度等，都可以通过这个实现 <br />
         例如：body { --theme: #ff6800; --background: rgba(255,255,255,0.85) }'
  );
  $JCustomCSS->setAttribute('class', 'joe_content joe_global');
  $form->addInput($JCustomCSS);
  /* --------------------------------------- */
  $JCustomScript = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JCustomScript',
    NULL,
    NULL,
    '自定义JS（非必填）',
    '介绍：请填写自定义JS内容，例如网站统计等，填写时无需填写script标签。'
  );
  $JCustomScript->setAttribute('class', 'joe_content joe_global');
  $form->addInput($JCustomScript);
  /* --------------------------------------- */
  $JCustomHeadEnd = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JCustomHeadEnd',
    NULL,
    NULL,
    '自定义增加&lt;head&gt;&lt;/head&gt;里内容（非必填）',
    '介绍：此处用于在&lt;head&gt;&lt;/head&gt;标签里增加自定义内容 <br />
         例如：可以填写引入第三方css、js等等'
  );
  $JCustomHeadEnd->setAttribute('class', 'joe_content joe_global');
  $form->addInput($JCustomHeadEnd);
  /* --------------------------------------- */
  $JCustomBodyEnd = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JCustomBodyEnd',
    NULL,
    NULL,
    '自定义&lt;body&gt;&lt;/body&gt;末尾位置内容（非必填）',
    '介绍：此处用于填写在&lt;body&gt;&lt;/body&gt;标签末尾位置的内容 <br>
         例如：可以填写引入第三方js脚本等等'
  );
  $JCustomBodyEnd->setAttribute('class', 'joe_content joe_global');
  $form->addInput($JCustomBodyEnd);
  /* --------------------------------------- */
  $JBirthDay = new Typecho_Widget_Helper_Form_Element_Text(
    'JBirthDay',
    NULL,
    NULL,
    '网站成立日期（非必填）',
    '介绍：用于显示当前站点已经运行了多少时间。<br>
         注意：填写时务必保证填写正确！例如：2023/1/1 00:00:00 <br>
         其他：不填写则不显示，若填写错误，则不会显示计时'
  );
  $JBirthDay->setAttribute('class', 'joe_content joe_global');
  $form->addInput($JBirthDay);
  /* --------------------------------------- */
  $JCustomFont = new Typecho_Widget_Helper_Form_Element_Text(
    'JCustomFont',
    NULL,
    NULL,
    '自定义网站字体（非必填）',
    '介绍：用于修改全站字体，填写则使用引入的字体，不填写使用默认字体 <br>
         格式：字体URL链接（推荐使用woff格式的字体，网页专用字体格式） <br>
         注意：字体文件一般有几兆，建议使用cdn链接'
  );
  $JCustomFont->setAttribute('class', 'joe_content joe_global');
  $form->addInput($JCustomFont);
  /* --------------------------------------- */
  $JCustomAvatarSource = new Typecho_Widget_Helper_Form_Element_Text(
    'JCustomAvatarSource',
    NULL,
    NULL,
    '自定义头像源（非必填）',
    '介绍：用于修改全站头像源地址 <br>
         例如：https://gravatar.zeruns.tech/avatar/ <br>
         其他：非必填，默认头像源为https://gravatar.helingqi.com/wavatar/ <br>
         注意：填写时，务必保证最后有一个/字符，否则不起作用！'
  );
  $JCustomAvatarSource->setAttribute('class', 'joe_content joe_global');
  $form->addInput($JCustomAvatarSource);
  /* --------------------------------------- */
  $JAside_Author_Nick = new Typecho_Widget_Helper_Form_Element_Text(
    'JAside_Author_Nick',
    NULL,
    "Typecho",
    '博主栏博主昵称 - PC/WAP',
    '介绍：用于修改博主栏的博主昵称 <br />
         注意：如果不填写时则显示 *个人设置* 里的昵称'
  );
  $JAside_Author_Nick->setAttribute('class', 'joe_content joe_aside');
  $form->addInput($JAside_Author_Nick);
  /* --------------------------------------- */
  $JAside_Author_Avatar = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JAside_Author_Avatar',
    NULL,
    NULL,
    '博主栏博主头像 - PC/WAP',
    '介绍：用于修改博主栏的博主头像 <br />
         注意：如果不填写时则显示 *个人设置* 里的头像'
  );
  $JAside_Author_Avatar->setAttribute('class', 'joe_content joe_aside');
  $form->addInput($JAside_Author_Avatar);
  /* --------------------------------------- */
  $JAside_Author_Image = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JAside_Author_Image',
    NULL,
    "https://npm.elemecdn.com/typecho-joe-latest/assets/img/aside_author_image.jpg",
    '博主栏背景壁纸 - PC',
    '介绍：用于修改PC端博主栏的背景壁纸 <br/>
         格式：图片地址 或 Base64地址'
  );
  $JAside_Author_Image->setAttribute('class', 'joe_content joe_aside');
  $form->addInput($JAside_Author_Image);
  /* --------------------------------------- */
  $JAside_Wap_Image = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JAside_Wap_Image',
    NULL,
    "https://npm.elemecdn.com/typecho-joe-latest/assets/img/wap_aside_image.jpg",
    '博主栏背景壁纸 - WAP',
    '介绍：用于修改WAP端博主栏的背景壁纸 <br/>
         格式：图片地址 或 Base64地址'
  );
  $JAside_Wap_Image->setAttribute('class', 'joe_content joe_aside');
  $form->addInput($JAside_Wap_Image);
  /* --------------------------------------- */
  $JAside_Author_Link = new Typecho_Widget_Helper_Form_Element_Text(
    'JAside_Author_Link',
    NULL,
    "https://blog.zhuxu.xyz/",
    '博主栏昵称跳转地址 - PC/WAP',
    '介绍：用于修改博主栏点击博主昵称后的跳转地址'
  );
  $JAside_Author_Link->setAttribute('class', 'joe_content joe_aside');
  $form->addInput($JAside_Author_Link);
  /* --------------------------------------- */
  $JAside_Author_Motto = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JAside_Author_Motto',
    NULL,
    "有钱终成眷属，没钱亲眼目睹",
    '博主栏座右铭（一言）- PC/WAP',
    '介绍：用于修改博主栏的座右铭（一言） <br />
         格式：可以填写多行也可以填写一行，填写多行时，每次随机显示其中的某一条，也可以填写API地址 <br />
         其他：API和自定义的座右铭完全可以一起写（换行填写），不会影响 <br />
         注意：API需要开启跨域权限才能调取，否则会调取失败！<br />
         推荐API：https://api.vvhan.com/api/ian'
  );
  $JAside_Author_Motto->setAttribute('class', 'joe_content joe_aside');
  $form->addInput($JAside_Author_Motto);
  /* --------------------------------------- */
  $JAside_Timelife_Status = new Typecho_Widget_Helper_Form_Element_Select(
    'JAside_Timelife_Status',
    array(
      'off' => '关闭（默认）',
      'on' => '开启'
    ),
    'off',
    '是否开启人生倒计时模块 - PC',
    '介绍：用于控制是否显示人生倒计时模块'
  );
  $JAside_Timelife_Status->setAttribute('class', 'joe_content joe_aside');
  $form->addInput($JAside_Timelife_Status->multiMode());
  /* --------------------------------------- */
  $JAside_Hot_Num = new Typecho_Widget_Helper_Form_Element_Select(
    'JAside_Hot_Num',
    array(
      'off' => '关闭（默认）',
      '3' => '显示3条',
      '4' => '显示4条',
      '5' => '显示5条',
      '6' => '显示6条',
      '7' => '显示7条',
      '8' => '显示8条',
      '9' => '显示9条',
      '10' => '显示10条',
    ),
    'off',
    '是否开启热门文章栏 - PC',
    '介绍：用于控制是否开启热门文章栏'
  );
  $JAside_Hot_Num->setAttribute('class', 'joe_content joe_aside');
  $form->addInput($JAside_Hot_Num->multiMode());
  /* --------------------------------------- */
  $JAside_Newreply_Status = new Typecho_Widget_Helper_Form_Element_Select(
    'JAside_Newreply_Status',
    array(
      'off' => '关闭（默认）',
      'on' => '开启'
    ),
    'off',
    '是否开启最新回复栏 - PC',
    '介绍：用于控制是否开启最新回复栏 <br>
         注意：如果您关闭了全站评论，将不会显示最新回复！'
  );
  $JAside_Newreply_Status->setAttribute('class', 'joe_content joe_aside');
  $form->addInput($JAside_Newreply_Status->multiMode());
  /* --------------------------------------- */
  $JAside_Weather_Key = new Typecho_Widget_Helper_Form_Element_Text(
    'JAside_Weather_Key',
    NULL,
    NULL,
    '天气栏KEY值 - PC',
    '介绍：用于初始化天气栏 <br/>
         注意：填写时务必填写正确！不填写则不会显示<br />
         其他：免费申请地址：<a href="//widget.qweather.com/create-standard">widget.qweather.com/create-standard</a><br />
         简要：在网页生成时，配置项随便选择，只需要生成代码后的Token即可'
  );
  $JAside_Weather_Key->setAttribute('class', 'joe_content joe_aside');
  $form->addInput($JAside_Weather_Key);
  /* --------------------------------------- */
  $JAside_Weather_Style = new Typecho_Widget_Helper_Form_Element_Select(
    'JAside_Weather_Style',
    array(
      '1' => '自动（默认）',
      '2' => '浅色',
      '3' => '深色'
    ),
    '1',
    '选择天气栏的风格 - PC',
    '介绍：选择一款您所喜爱的天气风格 <br />
         注意：需要先填写天气的KEY值才会生效'
  );
  $JAside_Weather_Style->setAttribute('class', 'joe_content joe_aside');
  $form->addInput($JAside_Weather_Style->multiMode());
  /* --------------------------------------- */
  $JADContent = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JADContent',
    NULL,
    NULL,
    '侧边栏广告 - PC',
    '介绍：用于设置侧边栏广告 <br />
         格式：广告图片 || 跳转链接 （中间使用两个竖杠分隔）<br />
         注意：如果您只想显示图片不想跳转，可填写：广告图片 || javascript:void(0)'
  );
  $JADContent->setAttribute('class', 'joe_content joe_aside');
  $form->addInput($JADContent);
  /* --------------------------------------- */
  $JCustomAside = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JCustomAside',
    NULL,
    NULL,
    '自定义侧边栏模块 - PC',
    '介绍：用于自定义侧边栏模块 <br />
         格式：请填写前端代码，不会写请勿填写 <br />
         例如：您可以在此处添加一个搜索框、时间、宠物、恋爱计时等等'
  );
  $JCustomAside->setAttribute('class', 'joe_content joe_aside');
  $form->addInput($JCustomAside);
  /* --------------------------------------- */
  $JAside_Tag = new Typecho_Widget_Helper_Form_Element_Select(
    'JAside_Tag',
    array(
        'off' => '关闭（默认）',
        '1' => '3D云标签',
        '2' => '普通标签',
    ),
    'off',
    '是否开启标签 - PC',
    '介绍：用于设置侧边栏是否显示标签'
  );
  $JAside_Tag->setAttribute('class', 'joe_content joe_aside');
  $form->addInput($JAside_Tag->multiMode());
  /* --------------------------------------- */
  $JAside_Flatterer = new Typecho_Widget_Helper_Form_Element_Select(
    'JAside_Flatterer',
    array(
      'off' => '关闭（默认）',
      'on' => '开启'
    ),
    'off',
    '是否开启舔狗日记 - PC',
    '介绍：用于设置侧边栏是否显示舔狗日记'
  );
  $JAside_Flatterer->setAttribute('class', 'joe_content joe_aside');
  $form->addInput($JAside_Flatterer->multiMode());
  /* --------------------------------------- */
  $JAside_History_Today = new Typecho_Widget_Helper_Form_Element_Select(
    'JAside_History_Today',
    array(
      'off' => '关闭（默认）',
      'on' => '开启'
    ),
    'off',
    '是否开启那年今日 - PC',
    '介绍：用于设置侧边栏是否显示往年今日的文章 <br />
         其他：如果往年今日有文章则显示，没有则不显示！'
  );
  $JAside_History_Today->setAttribute('class', 'joe_content joe_aside');
  $form->addInput($JAside_History_Today->multiMode());
  /* --------------------------------------- */
  $JThumbnail = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JThumbnail',
    NULL,
    NULL,
    '自定义缩略图',
    '介绍：用于修改主题默认缩略图 <br/>
         格式：图片地址，一行一个 <br />
         注意：不填写时，则使用主题内置的默认缩略图
         '
  );
  $JThumbnail->setAttribute('class', 'joe_content joe_image');
  $form->addInput($JThumbnail);
  /* --------------------------------------- */
  $JLazyload = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JLazyload',
    NULL,
    "https://npm.elemecdn.com/typecho-joe-latest/assets/img/lazyload.jpg",
    '自定义懒加载图',
    '介绍：用于修改主题默认懒加载图 <br/>
         格式：图片地址'
  );
  $JLazyload->setAttribute('class', 'joe_content joe_image');
  $form->addInput($JLazyload);
  /* --------------------------------------- */
  $JDynamic_Background = new Typecho_Widget_Helper_Form_Element_Select(
    'JDynamic_Background',
    array(
      'off' => '关闭（默认）',
      'backdrop1.js' => '效果1',
      'backdrop2.js' => '效果2',
      'backdrop3.js' => '效果3',
      'backdrop4.js' => '效果4',
      'backdrop5.js' => '效果5',
      'backdrop6.js' => '效果6'
    ),
    'off',
    '是否开启动态背景图（仅限PC）',
    '介绍：用于设置PC端动态背景<br />
         注意：如果您填写了下方PC端静态壁纸，将优先展示下方静态壁纸！如需显示动态壁纸，请将PC端静态壁纸设置成空'
  );
  $JDynamic_Background->setAttribute('class', 'joe_content joe_image');
  $form->addInput($JDynamic_Background->multiMode());
  /* --------------------------------------- */
  $JWallpaper_Background_PC = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JWallpaper_Background_PC',
    NULL,
    NULL,
    'PC端网站背景图片（非必填）',
    '介绍：PC端网站的背景图片，不填写时显示默认的灰色。<br />
         格式：图片URL地址 或 随机图片api 例如：https://api.btstu.cn/sjbz/?lx=dongman <br />
         注意：如果需要显示上方动态壁纸，请不要填写此项，此项优先级最高！'
  );
  $JWallpaper_Background_PC->setAttribute('class', 'joe_content joe_image');
  $form->addInput($JWallpaper_Background_PC);
  /* --------------------------------------- */
  $JWallpaper_Background_WAP = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JWallpaper_Background_WAP',
    NULL,
    NULL,
    'WAP端网站背景图片（非必填）',
    '介绍：WAP端网站的背景图片，不填写时显示默认的灰色。<br />
         格式：图片URL地址 或 随机图片api 例如：https://api.btstu.cn/sjbz/?lx=m_dongman'
  );
  $JWallpaper_Background_WAP->setAttribute('class', 'joe_content joe_image');
  $form->addInput($JWallpaper_Background_WAP);
  /* --------------------------------------- */
  $JIndex_Carousel = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JIndex_Carousel',
    NULL,
    NULL,
    '首页轮播图',
    '介绍：用于显示首页轮播图，请务必填写正确的格式 <br />
         格式：图片地址 || 跳转链接 || 标题 （中间使用两个竖杠分隔）<br />
         其他：一行一个，一行代表一个轮播图 <br />
         例如：<br />
            https://puui.qpic.cn/media_img/lena/PICykqaoi_580_1680/0 || https://baidu.com || 百度一下 <br />
            https://puui.qpic.cn/tv/0/1223447268_1680580/0 || https://v.qq.com || 腾讯视频
         '
  );
  $JIndex_Carousel->setAttribute('class', 'joe_content joe_index');
  $form->addInput($JIndex_Carousel);
  /* --------------------------------------- */
  $JIndexSticky = new Typecho_Widget_Helper_Form_Element_Text(
    'JIndexSticky',
    NULL,
    NULL,
    '首页置顶文章（非必填）',
    '介绍：请务必填写正确的格式 <br />
         格式：文章的ID || 文章的ID || 文章的ID （中间使用两个竖杠分隔）<br />
         例如：1 || 2 || 3'
  );
  $JIndexSticky->setAttribute('class', 'joe_content joe_index');
  $form->addInput($JIndexSticky);
  /* --------------------------------------- */
  $JIndex_Notice = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JIndex_Notice',
    NULL,
    NULL,
    '首页通知文字（非必填）',
    '介绍：请务必填写正确的格式 <br />
         格式：通知文字 || 跳转链接（中间使用两个竖杠分隔，限制一个）<br />
         例如：QQ群：151407331 || https://blog.zhuxu.xyz/'
  );
  $JIndex_Notice->setAttribute('class', 'joe_content joe_index');
  $form->addInput($JIndex_Notice);
  /* --------------------------------------- */
  $JFriends = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JFriends',
    NULL,
    '玖久的小窝 || https://blog.zhuxu.xyz/ || https://blog.zhuxu.xyz/assets/logo.png || 分享美好生活',
    '友情链接（非必填）',
    '介绍：用于填写友情链接 <br />
         注意：您需要先增加友链链接页面（新增独立页面-右侧模板选择友链），该项才会生效 <br />
         格式：博客名称 || 博客地址 || 博客头像 || 博客简介 <br />
         其他：一行一个，一行代表一个友链'
  );
  $JFriends->setAttribute('class', 'joe_content joe_other');
  $form->addInput($JFriends);
  /* --------------------------------------- */
  $JCustomPlayer = new Typecho_Widget_Helper_Form_Element_Text(
    'JCustomPlayer',
    NULL,
    NULL,
    '自定义视频播放器（非必填）',
    '介绍：用于修改主题自带的默认播放器 <br />
         例如：https://v.ini0.com/player/?url= <br />
         注意：主题自带的播放器只能解析M3U8的视频格式'
  );
  $JCustomPlayer->setAttribute('class', 'joe_content joe_other');
  $form->addInput($JCustomPlayer);
  /* --------------------------------------- */
  $JSensitiveWords = new Typecho_Widget_Helper_Form_Element_Textarea(
    'JSensitiveWords',
    NULL,
    '你妈死了 || 傻逼 || 操你妈 || 射你妈一脸',
    '评论敏感词（非必填）',
    '介绍：用于设置评论敏感词汇，如果用户评论包含这些词汇，则将会把评论置为审核状态 <br />
         例如：你妈死了 || 你妈炸了 || 我是你爹 || 你妈坟头冒烟 （多个使用 || 分隔开）'
  );
  $JSensitiveWords->setAttribute('class', 'joe_content joe_other');
  $form->addInput($JSensitiveWords);
  /* --------------------------------------- */
  $JLimitOneChinese = new Typecho_Widget_Helper_Form_Element_Select(
    'JLimitOneChinese',
    array('off' => '关闭（默认）', 'on' => '开启'),
    'off',
    '是否开启评论至少包含一个中文',
    '介绍：开启后如果评论内容未包含一个中文，则将会把评论置为审核状态 <br />
         其他：用于屏蔽国外机器人刷的全英文垃圾广告信息'
  );
  $JLimitOneChinese->setAttribute('class', 'joe_content joe_other');
  $form->addInput($JLimitOneChinese->multiMode());
  /* --------------------------------------- */
  $JTextLimit = new Typecho_Widget_Helper_Form_Element_Text(
    'JTextLimit',
    NULL,
    NULL,
    '限制用户评论最大字符',
    '介绍：如果用户评论的内容超出字符限制，则将会把评论置为审核状态 <br />
         其他：请输入数字格式，不填写则不限制'
  );
  $JTextLimit->setAttribute('class', 'joe_content joe_other');
  $form->addInput($JTextLimit->multiMode());
  /* --------------------------------------- */
  $JSiteMap = new Typecho_Widget_Helper_Form_Element_Select(
    'JSiteMap',
    array(
      'off' => '关闭（默认）',
      '100' => '显示最新 100 条链接',
      '200' => '显示最新 200 条链接',
      '300' => '显示最新 300 条链接',
      '400' => '显示最新 400 条链接',
      '500' => '显示最新 500 条链接',
      '600' => '显示最新 600 条链接',
      '700' => '显示最新 700 条链接',
      '800' => '显示最新 800 条链接',
      '900' => '显示最新 900 条链接',
      '1000' => '显示最新 1000 条链接',
    ),
    'off',
    '是否开启主题自带SiteMap功能',
    '介绍：开启后博客将享有SiteMap功能 <br />
         其他：链接为博客最新实时链接 <br />
         好处：无需手动生成，无需频繁提交，提交一次即可 <br />
         开启后SiteMap访问地址：<br />
         http(s)://域名/sitemap.xml （开启了伪静态）<br />  
         http(s)://域名/index.php/sitemap.xml （未开启伪静态）
         '
  );
  $JSiteMap->setAttribute('class', 'joe_content joe_other');
  $form->addInput($JSiteMap->multiMode());
  /* --------------------------------------- */
  $JPageStatus = new Typecho_Widget_Helper_Form_Element_Select(
    'JPageStatus',
    array('default' => '按钮切换形式（默认）', 'ajax' => '点击加载形式'),
    'default',
    '选择首页的分页形式',
    '介绍：选择一款您所喜欢的分页形式'
  );
  $JPageStatus->setAttribute('class', 'joe_content joe_other');
  $form->addInput($JPageStatus->multiMode());
  /* --------------------------------------- */
  $JBTPanel = new Typecho_Widget_Helper_Form_Element_Text(
    'JBTPanel',
    NULL,
    NULL,
    '宝塔面板地址',
    '介绍：用于统计页面获取服务器状态使用 <br>
         例如：http://192.168.1.245:8888/ <br>
         注意：结尾需要带有一个 / 字符！<br>
         该功能需要去宝塔面板开启开放API，并添加白名单才可使用'
  );
  $JBTPanel->setAttribute('class', 'joe_content joe_other');
  $form->addInput($JBTPanel->multiMode());
  /* --------------------------------------- */
  $JBTKey = new Typecho_Widget_Helper_Form_Element_Text(
    'JBTKey',
    NULL,
    NULL,
    '宝塔开放接口密钥',
    '介绍：用于统计页面获取服务器状态使用 <br>
         例如：thVLXFtUCCNzBShBweKTPBmw8296q8R8 <br>
         该功能需要去宝塔面板开启开放API，并添加白名单才可使用'
  );
  $JBTKey->setAttribute('class', 'joe_content joe_other');
  $form->addInput($JBTKey->multiMode());
  /* --------------------------------------- */
  $JCommentMail = new Typecho_Widget_Helper_Form_Element_Select(
    'JCommentMail',
    array('off' => '关闭（默认）', 'on' => '开启'),
    'off',
    '是否开启评论邮件通知',
    '介绍：开启后评论内容将会进行邮箱通知 <br />
         注意：此项需要您完整无错的填写下方的邮箱设置！！ <br />
         其他：下方例子以QQ邮箱为例，推荐使用QQ邮箱'
  );
  $JCommentMail->setAttribute('class', 'joe_content joe_other');
  $form->addInput($JCommentMail->multiMode());
  /* --------------------------------------- */
  $JCommentMailHost = new Typecho_Widget_Helper_Form_Element_Text(
    'JCommentMailHost',
    NULL,
    NULL,
    '邮箱服务器地址',
    '例如：smtp.qq.com'
  );
  $JCommentMailHost->setAttribute('class', 'joe_content joe_other');
  $form->addInput($JCommentMailHost->multiMode());
  /* --------------------------------------- */
  $JCommentSMTPSecure = new Typecho_Widget_Helper_Form_Element_Select(
    'JCommentSMTPSecure',
    array('ssl' => 'ssl（默认）', 'tsl' => 'tsl'),
    'ssl',
    '加密方式',
    '介绍：用于选择登录鉴权加密方式'
  );
  $JCommentSMTPSecure->setAttribute('class', 'joe_content joe_other');
  $form->addInput($JCommentSMTPSecure->multiMode());
  /* --------------------------------------- */
  $JCommentMailPort = new Typecho_Widget_Helper_Form_Element_Text(
    'JCommentMailPort',
    NULL,
    NULL,
    '邮箱服务器端口号',
    '例如：465'
  );
  $JCommentMailPort->setAttribute('class', 'joe_content joe_other');
  $form->addInput($JCommentMailPort->multiMode());
  /* --------------------------------------- */
  $JCommentMailFromName = new Typecho_Widget_Helper_Form_Element_Text(
    'JCommentMailFromName',
    NULL,
    NULL,
    '发件人昵称',
    '例如：帅气的象拔蚌'
  );
  $JCommentMailFromName->setAttribute('class', 'joe_content joe_other');
  $form->addInput($JCommentMailFromName->multiMode());
  /* --------------------------------------- */
  $JCommentMailAccount = new Typecho_Widget_Helper_Form_Element_Text(
    'JCommentMailAccount',
    NULL,
    NULL,
    '发件人邮箱',
    '例如：2323333339@qq.com'
  );
  $JCommentMailAccount->setAttribute('class', 'joe_content joe_other');
  $form->addInput($JCommentMailAccount->multiMode());
  /* --------------------------------------- */
  $JCommentMailPassword = new Typecho_Widget_Helper_Form_Element_Text(
    'JCommentMailPassword',
    NULL,
    NULL,
    '邮箱授权码',
    '介绍：这里填写的是邮箱生成的授权码 <br>
         获取方式（以QQ邮箱为例）：<br>
         QQ邮箱 > 设置 > 账户 > IMAP/SMTP服务 > 开启 <br>
         其他：这个可以百度一下开启教程，有图文教程'
  );
  $JCommentMailPassword->setAttribute('class', 'joe_content joe_other');
  $form->addInput($JCommentMailPassword->multiMode());
  /* --------------------------------------- */
  $JBaiduToken = new Typecho_Widget_Helper_Form_Element_Text(
    'JBaiduToken',
    NULL,
    NULL,
    '百度推送Token',
    '介绍：填写此处，前台文章页如果未收录，则会自动将当前链接推送给百度加快收录 <br />
         其他：Token在百度收录平台注册账号获取'
  );
  $JBaiduToken->setAttribute('class', 'joe_content joe_post');
  $form->addInput($JBaiduToken);
  /* --------------------------------------- */
  $JOverdue = new Typecho_Widget_Helper_Form_Element_Select(
    'JOverdue',
    array(
      'off' => '关闭（默认）',
      '3' => '大于3天',
      '7' => '大于7天',
      '15' => '大于15天',
      '30' => '大于30天',
      '60' => '大于60天',
      '90' => '大于90天',
      '120' => '大于120天',
      '180' => '大于180天'
    ),
    'off',
    '是否开启文章更新时间大于多少天提示（仅针对文章有效）',
    '介绍：开启后如果文章在多少天内无任何修改，则进行提示'
  );
  $JOverdue->setAttribute('class', 'joe_content joe_post');
  $form->addInput($JOverdue->multiMode());
  /* --------------------------------------- */
  $JEditor = new Typecho_Widget_Helper_Form_Element_Select(
    'JEditor',
    array(
      'on' => '开启（默认）',
      'off' => '关闭',
    ),
    'on',
    '是否启用Joe自定义编辑器',
    '介绍：开启后，文章编辑器将替换成Joe编辑器 <br>
         其他：目前编辑器处于拓展阶段，如果想继续使用原生编辑器，关闭此项即可'
  );
  $JEditor->setAttribute('class', 'joe_content joe_post');
  $form->addInput($JEditor->multiMode());
  /* --------------------------------------- */
  $JPrismTheme = new Typecho_Widget_Helper_Form_Element_Select(
    'JPrismTheme',
    array(
      '//npm.elemecdn.com/prismjs@1.29.0/themes/prism.min.css' => 'prism（默认）',
      '//npm.elemecdn.com/prismjs@1.29.0/themes/prism-dark.min.css' => 'prism-dark',
      '//npm.elemecdn.com/prismjs@1.29.0/themes/prism-okaidia.min.css' => 'prism-okaidia',
      '//npm.elemecdn.com/prismjs@1.29.0/themes/prism-solarizedlight.min.css' => 'prism-solarizedlight',
      '//npm.elemecdn.com/prismjs@1.29.0/themes/prism-tomorrow.min.css' => 'prism-tomorrow',
      '//npm.elemecdn.com/prismjs@1.29.0/themes/prism-twilight.min.css' => 'prism-twilight',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-a11y-dark.min.css' => 'prism-a11y-dark',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-atom-dark.min.css' => 'prism-atom-dark',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-base16-ateliersulphurpool.light.min.css' => 'prism-base16-ateliersulphurpool.light',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-cb.min.css' => 'prism-cb',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-coldark-cold.min.css' => 'prism-coldark-cold',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-coldark-dark.min.css' => 'prism-coldark-dark',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-darcula.min.css' => 'prism-darcula',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-dracula.min.css' => 'prism-dracula',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-duotone-dark.min.css' => 'prism-duotone-dark',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-duotone-earth.min.css' => 'prism-duotone-earth',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-duotone-forest.min.css' => 'prism-duotone-forest',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-duotone-light.min.css' => 'prism-duotone-light',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-duotone-sea.min.css' => 'prism-duotone-sea',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-duotone-space.min.css' => 'prism-duotone-space',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-ghcolors.min.css' => 'prism-ghcolors',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-gruvbox-dark.min.css' => 'prism-gruvbox-dark',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-hopscotch.min.css' => 'prism-hopscotch',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-lucario.min.css' => 'prism-lucario',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-material-dark.min.css' => 'prism-material-dark',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-material-light.min.css' => 'prism-material-light',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-material-oceanic.min.css' => 'prism-material-oceanic',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-night-owl.min.css' => 'prism-night-owl',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-nord.min.css' => 'prism-nord',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-pojoaque.min.css' => 'prism-pojoaque',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-shades-of-purple.min.css' => 'prism-shades-of-purple',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-synthwave84.min.css' => 'prism-synthwave84',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-vs.min.css' => 'prism-vs',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-vsc-dark-plus.min.css' => 'prism-vsc-dark-plus',
      '//npm.elemecdn.com/prism-themes@1.9.0/themes/prism-xonokai.min.css' => 'prism-xonokai',
      '//npm.elemecdn.com/prism-theme-one-light-dark@1.0.4/prism-onelight.min.css' => 'prism-onelight',
      '//npm.elemecdn.com/prism-theme-one-light-dark@1.0.4/prism-onedark.min.css' => 'prism-onedark',
      '//npm.elemecdn.com/prism-theme-one-dark@1.0.0/prism-onedark.min.css' => 'prism-onedark2',
    ),
    '//npm.elemecdn.com/prismjs@1.29.0/themes/prism.min.css',
    '选择一款您喜欢的代码高亮样式',
    '介绍：用于修改代码块的高亮风格 <br>
         其他：如果您有其他样式，可通过源代码修改此项，引入您的自定义样式链接'
  );
  $JPrismTheme->setAttribute('class', 'joe_content joe_post');
  $form->addInput($JPrismTheme->multiMode());
} ?>
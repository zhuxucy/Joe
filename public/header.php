<header class="joe_header <?php echo $this->is('post') ? 'current' : '' ?>">
  <div class="joe_header__above">
    <div class="joe_container">
      <svg class="joe_header__above-slideicon" viewBox="0 0 1152 1024" xmlns="http://www.w3.org/2000/svg" width="20" height="20">
        <path d="M76.032 872a59.968 59.968 0 1 0 0 120h999.936a59.968 59.968 0 1 0 0-120H76.032zm16-420.032a59.968 59.968 0 1 0 0 120h599.936a59.968 59.968 0 1 0 0-119.936H92.032zM76.032 32a59.968 59.968 0 1 0 0 120h999.936a60.032 60.032 0 0 0 0-120H76.032z" />
      </svg>
      <a title="<?php $this->options->title(); ?>" class="joe_header__above-logo" href="<?php $this->options->siteUrl(); ?>">
        <img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php $this->options->JLogo() ?>" alt="<?php $this->options->title(); ?>" />
        <svg class="profile-color-modes" height="45" viewBox="0 0 106 60" fill="none" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" xmlns="http://www.w3.org/2000/svg">
          <g class="profile-color-modes-illu-group profile-color-modes-illu-red">
            <path d="M37.5 58.5V57.5C37.5 49.768 43.768 43.5 51.5 43.5V43.5C59.232 43.5 65.5 49.768 65.5 57.5V58.5"></path>
          </g>
          <g class="profile-color-modes-illu-group profile-color-modes-illu-orange">
            <path d="M104.07 58.5C103.401 55.092 97.7635 54.3869 95.5375 57.489C97.4039 54.6411 99.7685 48.8845 94.6889 46.6592C89.4817 44.378 86.1428 50.1604 85.3786 54.1158C85.9519 50.4768 83.7226 43.294 78.219 44.6737C72.7154 46.0534 72.7793 51.3754 74.4992 55.489C74.169 54.7601 72.4917 53.3567 70.5 52.8196"></path>
          </g>
          <g class="profile-color-modes-illu-group profile-color-modes-illu-purple">
            <path d="M5.51109 58.5V52.5C5.51109 41.4543 14.4654 32.5 25.5111 32.5C31.4845 32.5 36.8464 35.1188 40.5111 39.2709C40.7212 39.5089 40.9258 39.7521 41.1245 40"></path>
            <path d="M27.511 49.5C29.6777 49.5 28.911 49.5 32.511 49.5"></path>
            <path d="M27.511 56.5C29.6776 56.5 26.911 56.5 30.511 56.5"></path>
          </g>
          <g class="profile-color-modes-illu-group profile-color-modes-illu-green">
            <circle cx="5.5" cy="12.5" r="4"></circle>
            <circle cx="18.5" cy="5.5" r="4"></circle>
            <path d="M18.5 9.5L18.5 27.5"></path>
            <path d="M18.5 23.5C6 23.5 5.5 23.6064 5.5 16.5"></path>
          </g>
          <g class="profile-color-modes-illu-group profile-color-modes-illu-blue">
            <g class="profile-color-modes-illu-frame">
              <path d="M40.6983 31.5C40.5387 29.6246 40.6456 28.0199 41.1762 27.2317C42.9939 24.5312 49.7417 26.6027 52.5428 30.2409C54.2551 29.8552 56.0796 29.6619 57.9731 29.6619C59.8169 29.6619 61.5953 29.8452 63.2682 30.211C66.0833 26.5913 72.799 24.5386 74.6117 27.2317C75.6839 28.8246 75.0259 33.7525 73.9345 37.5094C74.2013 37.9848 74.4422 38.4817 74.6555 39"></path>
            </g>
            <g class="profile-color-modes-illu-frame">
              <path d="M41.508 31.5C41.6336 31.2259 41.7672 30.9582 41.9085 30.6968C40.7845 26.9182 40.086 21.8512 41.1762 20.2317C42.9939 17.5312 49.7417 19.6027 52.5428 23.2409C54.2551 22.8552 56.0796 22.6619 57.9731 22.6619C59.8169 22.6619 61.5953 22.8452 63.2682 23.211C66.0833 19.5913 72.799 17.5386 74.6117 20.2317C75.6839 21.8246 75.0259 26.7525 73.9345 30.5094C75.1352 32.6488 75.811 35.2229 75.811 38.2283C75.811 38.49 75.8058 38.7472 75.7957 39"></path>
              <path d="M49.4996 33V35.6757"></path>
              <path d="M67.3375 33V35.6757"></path>
            </g>
            <g class="profile-color-modes-illu-frame">
              <path d="M41.508 31.5C41.6336 31.2259 41.7672 30.9582 41.9085 30.6968C40.7845 26.9182 40.086 21.8512 41.1762 20.2317C42.9939 17.5312 49.7417 19.6027 52.5428 23.2409C54.2551 22.8552 56.0796 22.6619 57.9731 22.6619C59.8169 22.6619 61.5953 22.8452 63.2682 23.211C66.0833 19.5913 72.799 17.5386 74.6117 20.2317C75.6839 21.8246 75.0259 26.7525 73.9345 30.5094C75.1352 32.6488 75.811 35.2229 75.811 38.2283C75.811 38.49 75.8058 38.7472 75.7957 39"></path>
            </g>
            <g class="profile-color-modes-illu-frame">
              <path d="M41.508 31.5C41.6336 31.2259 41.7672 30.9582 41.9085 30.6968C40.7845 26.9182 40.086 21.8512 41.1762 20.2317C42.9939 17.5312 49.7417 19.6027 52.5428 23.2409C54.2551 22.8552 56.0796 22.6619 57.9731 22.6619C59.8169 22.6619 61.5953 22.8452 63.2682 23.211C66.0833 19.5913 72.799 17.5386 74.6117 20.2317C75.6839 21.8246 75.0259 26.7525 73.9345 30.5094C75.1352 32.6488 75.811 35.2229 75.811 38.2283C75.811 38.49 75.8058 38.7472 75.7957 39"></path>
              <path d="M49.4996 33V35.6757"></path>
              <path d="M67.3375 33V35.6757"></path>
            </g>
            <g class="profile-color-modes-illu-frame">
              <path d="M41.508 31.5C41.6336 31.2259 41.7672 30.9582 41.9085 30.6968C40.7845 26.9182 40.086 21.8512 41.1762 20.2317C42.9939 17.5312 49.7417 19.6027 52.5428 23.2409C54.2551 22.8552 56.0796 22.6619 57.9731 22.6619C59.8169 22.6619 61.5953 22.8452 63.2682 23.211C66.0833 19.5913 72.799 17.5386 74.6117 20.2317C75.6839 21.8246 75.0259 26.7525 73.9345 30.5094C75.1352 32.6488 75.811 35.2229 75.811 38.2283C75.811 38.49 75.8058 38.7472 75.7957 39"></path>
            </g>
            <g class="profile-color-modes-illu-frame">
              <path d="M41.508 31.5C41.6336 31.2259 41.7672 30.9582 41.9085 30.6968C40.7845 26.9182 40.086 21.8512 41.1762 20.2317C42.9939 17.5312 49.7417 19.6027 52.5428 23.2409C54.2551 22.8552 56.0796 22.6619 57.9731 22.6619C59.8169 22.6619 61.5953 22.8452 63.2682 23.211C66.0833 19.5913 72.799 17.5386 74.6117 20.2317C75.6839 21.8246 75.0259 26.7525 73.9345 30.5094C75.1352 32.6488 75.811 35.2229 75.811 38.2283C75.811 38.49 75.8058 38.7472 75.7957 39"></path>
              <path d="M49.4996 33V35.6757"></path>
              <path d="M67.3375 33V35.6757"></path>
            </g>
            <g class="profile-color-modes-illu-frame">
              <path d="M73.4999 40.2236C74.9709 38.2049 75.8108 35.5791 75.8108 32.2283C75.8108 29.2229 75.1351 26.6488 73.9344 24.5094C75.0258 20.7525 75.6838 15.8246 74.6116 14.2317C72.7989 11.5386 66.0832 13.5913 63.2681 17.211C61.5952 16.8452 59.8167 16.6619 57.973 16.6619C56.0795 16.6619 54.2549 16.8552 52.5427 17.2409C49.7416 13.6027 42.9938 11.5312 41.176 14.2317C40.0859 15.8512 40.7843 20.9182 41.9084 24.6968C41.003 26.3716 40.4146 28.3065 40.2129 30.5"></path>
              <path d="M82.9458 30.5471L76.8413 31.657"></path>
              <path d="M76.2867 34.4319L81.8362 37.7616"></path>
              <path d="M49.4995 27.8242V30.4999"></path>
              <path d="M67.3374 27.8242V30.4998"></path>
            </g>
            <g class="profile-color-modes-illu-frame">
              <path d="M45.3697 34.2658C41.8877 32.1376 39.7113 28.6222 39.7113 23.2283C39.7113 20.3101 40.3483 17.7986 41.4845 15.6968C40.3605 11.9182 39.662 6.85125 40.7522 5.23168C42.5699 2.53117 49.3177 4.6027 52.1188 8.24095C53.831 7.85521 55.6556 7.66186 57.5491 7.66186C59.3929 7.66186 61.1713 7.84519 62.8442 8.21095C65.6593 4.59134 72.375 2.5386 74.1877 5.23168C75.2599 6.82461 74.6019 11.7525 73.5105 15.5094C74.7112 17.6488 75.3869 20.2229 75.3869 23.2283C75.3869 28.6222 73.2105 32.1376 69.7285 34.2658C70.8603 35.5363 72.6057 38.3556 73.3076 40"></path>
              <path d="M49.0747 19.8242V22.4999"></path>
              <path d="M54.0991 28C54.6651 29.0893 55.7863 30.0812 57.9929 30.0812C59.0642 30.0812 59.8797 29.8461 60.5 29.4788"></path>
              <path d="M66.9126 19.8242V22.4999"></path>
              <path d="M33.2533 20.0237L39.0723 22.1767"></path>
              <path d="M39.1369 25.0058L33.0935 27.3212"></path>
              <path d="M81.8442 19.022L76.0252 21.1751"></path>
              <path d="M75.961 24.0041L82.0045 26.3196"></path>
            </g>
            <g class="profile-color-modes-illu-frame">
              <path d="M73.4999 40.2236C74.9709 38.2049 75.8108 35.5791 75.8108 32.2283C75.8108 29.2229 75.1351 26.6488 73.9344 24.5094C75.0258 20.7525 75.6838 15.8246 74.6116 14.2317C72.7989 11.5386 66.0832 13.5913 63.2681 17.211C61.5952 16.8452 59.8167 16.6619 57.973 16.6619C56.0795 16.6619 54.2549 16.8552 52.5427 17.2409C49.7416 13.6027 42.9938 11.5312 41.176 14.2317C40.0859 15.8512 40.7843 20.9182 41.9084 24.6968C41.003 26.3716 40.4146 28.3065 40.2129 30.5"></path>
              <path d="M82.9458 30.5471L76.8413 31.657"></path>
              <path d="M76.2867 34.4319L81.8362 37.7616"></path>
              <path d="M49.4995 27.8242V30.4999"></path>
              <path d="M67.3374 27.8242V30.4998"></path>
            </g>
            <g class="profile-color-modes-illu-frame">
              <path d="M40.6983 31.5C40.5387 29.6246 40.6456 28.0199 41.1762 27.2317C42.9939 24.5312 49.7417 26.6027 52.5428 30.2409C54.2551 29.8552 56.0796 29.6619 57.9731 29.6619C59.8169 29.6619 61.5953 29.8452 63.2682 30.211C66.0833 26.5913 72.799 24.5386 74.6117 27.2317C75.6839 28.8246 75.0259 33.7525 73.9345 37.5094C74.2013 37.9848 74.4422 38.4817 74.6555 39"></path>
            </g>
          </g>
        </svg>
      </a>
      <nav class="joe_header__above-nav">
        <a class="item <?php echo $this->is('index') ? 'active' : '' ?>" href="<?php $this->options->siteUrl(); ?>" title="首页">首页</a>
        <?php $this->widget('Widget_Contents_Page_List')->to($pages); ?>
        <?php if (count($pages->stack) <= $this->options->JNavMaxNum) : ?>
          <?php foreach ($pages->stack as $item) : ?>
            <a class="item <?php echo $this->is('page', $item['slug']) ? 'active' : '' ?>" href="<?php echo $item['permalink'] ?>" title="<?php echo $item['title'] ?>"><?php echo $item['title'] ?></a>
          <?php endforeach; ?>
        <?php else : ?>
          <?php foreach (array_slice($pages->stack, 0, $this->options->JNavMaxNum) as $item) : ?>
            <a class="item <?php echo $this->is('page', $item['slug']) ? 'active' : '' ?>" href="<?php echo $item['permalink'] ?>" title="<?php echo $item['title'] ?>"><?php echo $item['title'] ?></a>
          <?php endforeach; ?>
          <div class="joe_dropdown" trigger="hover" placement="60px" style="margin-right: 15px;">
            <div class="joe_dropdown__link">
              <a href="#" rel="nofollow">更多</a>
              <svg class="joe_dropdown__link-icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="14" height="14">
                <path d="M561.873 725.165c-11.262 11.262-26.545 21.72-41.025 18.502-14.479 2.413-28.154-8.849-39.415-18.502L133.129 375.252c-17.697-17.696-17.697-46.655 0-64.352s46.655-17.696 64.351 0l324.173 333.021 324.977-333.02c17.696-17.697 46.655-17.697 64.351 0s17.697 46.655 0 64.351L561.873 725.165z" p-id="3535" fill="var(--main)"></path>
              </svg>
            </div>
            <nav class="joe_dropdown__menu">
              <?php foreach (array_slice($pages->stack, $this->options->JNavMaxNum) as $item) : ?>
                <a class="<?php echo $this->is('page', $item['slug']) ? 'active' : '' ?>" href="<?php echo $item['permalink'] ?>" title="<?php echo $item['title'] ?>"><?php echo $item['title'] ?></a>
              <?php endforeach; ?>
            </nav>
          </div>
        <?php endif; ?>
        <?php
        $custom = [];
        $custom_text = $this->options->JCustomNavs;
        if ($custom_text) {
          $custom_arr = explode("\r\n", $custom_text);
          if (count($custom_arr) > 0) {
            for ($i = 0; $i < count($custom_arr); $i++) {
              $title = explode("||", $custom_arr[$i])[0];
              $url = explode("||", $custom_arr[$i])[1];
              $custom[] = array("title" => trim($title), "url" => trim($url));
            };
          }
        }
        ?>
        <?php if (sizeof($custom) > 0) : ?>
          <div class="joe_dropdown" trigger="hover" placement="60px">
            <div class="joe_dropdown__link">
              <a href="#" rel="nofollow">推荐</a>
              <svg class="joe_dropdown__link-icon" viewBox="0 0 1024 1024" xmlns="http://www.w3.org/2000/svg" width="14" height="14">
                <path d="M561.873 725.165c-11.262 11.262-26.545 21.72-41.025 18.502-14.479 2.413-28.154-8.849-39.415-18.502L133.129 375.252c-17.697-17.696-17.697-46.655 0-64.352s46.655-17.696 64.351 0l324.173 333.021 324.977-333.02c17.696-17.697 46.655-17.697 64.351 0s17.697 46.655 0 64.351L561.873 725.165z" fill="var(--main)" />
              </svg>
            </div>
            <nav class="joe_dropdown__menu">
              <?php foreach ($custom as $item) : ?>
                <a href="<?php echo $item['url'] ?>" target="_blank" rel="noopener noreferrer nofollow"><?php echo $item['title'] ?></a>
              <?php endforeach; ?>
            </nav>
          </div>
        <?php endif; ?>
      </nav>
      <form class="joe_header__above-search" method="post" action="<?php $this->options->siteUrl(); ?>">
        <input maxlength="16" autocomplete="off" placeholder="请输入关键字" name="s" value="<?php echo $this->is('search') ? $this->archiveTitle(' &raquo; ', '', '') : '' ?>" class="input" type="text" />
        <button type="submit" class="submit">搜索</button>
        <span class="icon"></span>
        <nav class="result">
          <?php $this->widget('Widget_Contents_Hot@Search', 'pageSize=5')->to($item); ?>
          <?php $index = 1; ?>
          <?php while ($item->next()) : ?>
          <?php $index++; ?>
          <?php endwhile; ?>
        </nav>
      </form>
      <svg class="joe_header__above-searchicon" viewBox="0 0 1024 1024" xmlns="http://www.w3.org/2000/svg" width="20" height="20">
        <path d="M1008.19 932.031L771.72 695.56a431.153 431.153 0 1 0-76.158 76.158l236.408 236.472a53.758 53.758 0 0 0 76.158 0 53.758 53.758 0 0 0 0-76.158zM107.807 431.185a323.637 323.637 0 0 1 323.316-323.381 323.7 323.7 0 0 1 323.381 323.38 323.637 323.637 0 0 1-323.38 323.317 323.637 323.637 0 0 1-323.317-323.316z" />
      </svg>
    </div>
  </div>

  <div class="joe_header__below">
    <div class="joe_container">
      <?php if ($this->is('post')) :  ?>
        <div class="joe_header__below-title"><?php $this->title() ?></div>
      <?php endif; ?>
      <nav class="joe_header__below-class">
        <?php $this->widget('Widget_Metas_Category_List')->to($category); ?>
        <?php while ($category->next()) : ?>
          <?php if ($category->levels === 0) : ?>
            <?php $children = $category->getAllChildren($category->mid); ?>
            <?php if (empty($children)) : ?>
              <a class="item <?php echo $this->is('category', $category->slug) ? 'active' : '' ?>" href="<?php $category->permalink(); ?>" title="<?php $category->name(); ?>"><?php $category->name(); ?></a>
            <?php else : ?>
              <div class="joe_dropdown" trigger="hover">
                <div class="joe_dropdown__link">
                  <a class="item <?php echo $this->is('category', $category->slug) ? 'active' : '' ?>" href="<?php $category->permalink(); ?>" title="<?php $category->name(); ?>"></i><?php $category->name(); ?></a>
                  <svg class="joe_dropdown__link-icon" viewBox="0 0 1024 1024" xmlns="http://www.w3.org/2000/svg" width="13" height="13">
                    <path d="M561.873 725.165c-11.262 11.262-26.545 21.72-41.025 18.502-14.479 2.413-28.154-8.849-39.415-18.502L133.129 375.252c-17.697-17.696-17.697-46.655 0-64.352s46.655-17.696 64.351 0l324.173 333.021 324.977-333.02c17.696-17.697 46.655-17.697 64.351 0s17.697 46.655 0 64.351L561.873 725.165z" fill="var(--minor)" />
                  </svg>
                </div>
                <nav class="joe_dropdown__menu">
                  <?php foreach ($children as $mid) : ?>
                    <?php $child = $category->getCategory($mid); ?>
                    <a class="<?php echo $this->is('category', $child['slug']) ? 'active' : '' ?>" href="<?php echo $child['permalink'] ?>" title="<?php echo $child['name']; ?>"><?php echo $child['name']; ?></a>
                  <?php endforeach; ?>
                </nav>
              </div>
            <?php endif; ?>
          <?php endif; ?>
        <?php endwhile; ?>
      </nav>
      <div class="joe_header__below-sign">
        <?php if ($this->user->hasLogin()) : ?>
          <div class="joe_dropdown" trigger="click">
            <div class="joe_dropdown__link">
             <span><?php $this->user->screenName(); ?></span>
            </div>
            <nav class="joe_dropdown__menu">
              <a rel="noopener noreferrer nofollow" target="_blank" href="<?php $this->options->adminUrl(); ?>">进入后台</a>
              <a href="<?php $this->options->logoutUrl(); ?>">退出登录</a>
            </nav>
          </div>
        <?php else : ?>
          <div class="item">
            <a href="<?php $this->options->adminUrl('login.php'); ?>" target="_blank" rel="noopener noreferrer nofollow">登录</a>
            <?php if ($this->options->allowRegister) : ?>
              <span class="split">/</span>
              <a href="<?php $this->options->adminUrl('register.php'); ?>" target="_blank" rel="noopener noreferrer nofollow">注册</a>
            <?php endif; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>

  <div class="joe_header__searchout">
    <div class="joe_container">
      <div class="joe_header__searchout-inner">
        <form class="search" method="post" action="<?php $this->options->siteUrl(); ?>">
          <input maxlength="16" autocomplete="off" placeholder="请输入关键字" name="s" value="<?php echo $this->is('search') ? $this->archiveTitle(' &raquo; ', '', '') : '' ?>" class="input" type="text" />
          <button type="submit" class="submit">搜索</button>
        </form>
      </div>
    </div>
  </div>

  <div class="joe_header__slideout">
    <img width="100%" height="150" class="joe_header__slideout-image" src="<?php $this->options->JAside_Wap_Image() ?>" alt="侧边栏壁纸" />
    <div class="joe_header__slideout-author">
      <img width="50" height="50" class="avatar lazyload" src="<?php _getAvatarLazyload(); ?>" data-src="<?php $this->options->JAside_Author_Avatar ? $this->options->JAside_Author_Avatar() : _getAvatarByMail($this->authorId ? $this->author->mail : $this->user->mail) ?>" alt="博主昵称" />
      <div class="info">
        <a class="link" href="<?php $this->options->JAside_Author_Link() ?>" target="_blank" rel="noopener noreferrer nofollow"><?php $this->options->JAside_Author_Nick ? $this->options->JAside_Author_Nick() : ($this->authorId ? $this->author->screenName() : $this->user->screenName()); ?></a>
        <p class="motto joe_motto"></p>
      </div>
    </div>
    <ul class="joe_header__slideout-count">
      <?php Typecho_Widget::widget('Widget_Stat')->to($count); ?>
      <li class="item">
        <i class="fa fa-book"></i>
        <span>累计撰写 <strong><?php echo number_format($count->publishedPostsNum); ?></strong> 篇文章</span>
      </li>
      <li class="item">
        <i class="fa fa-envelope"></i>
        <span>累计收到 <strong><?php echo number_format($count->publishedCommentsNum); ?></strong> 条评论</span>
      </li>
    </ul>
    <ul class="joe_header__slideout-menu panel-box">
      <li>
        <a class="link" href="<?php $this->options->siteUrl(); ?>" title="首页">
          <span><i class="fa fa-home"></i>首页</span>
        </a>
      </li>
      <!-- 分类 -->
      <li>
        <a class="link panel" href="#" rel="nofollow">
          <span><i class="fa fa-cube"></i>分类</span>
          <svg class="icon" viewBox="0 0 1024 1024" xmlns="http://www.w3.org/2000/svg" width="13" height="13">
            <path d="M624.865 512.247L332.71 220.088c-12.28-12.27-12.28-32.186 0-44.457 12.27-12.28 32.186-12.28 44.457 0l314.388 314.388c12.28 12.27 12.28 32.186 0 44.457L377.167 848.863c-6.136 6.14-14.183 9.211-22.228 9.211s-16.092-3.071-22.228-9.211c-12.28-12.27-12.28-32.186 0-44.457l292.155-292.16z" />
          </svg>
        </a>
        <ul class="slides panel-body panel-box">
          <?php while ($category->next()) : ?>
            <?php if ($category->levels === 0) : ?>
              <?php $children = $category->getAllChildren($category->mid); ?>
              <?php if (empty($children)) : ?>
                <li>
                  <a class="link <?php echo $this->is('category', $category->slug) ? 'current' : '' ?>" href="<?php $category->permalink(); ?>" title="<?php $category->name(); ?>"><?php $category->name(); ?></a>
                </li>
              <?php else : ?>
                <li>
                  <div class="link panel <?php echo $this->is('category', $category->slug) ? 'current' : '' ?>">
                    <a href="<?php $category->permalink(); ?>" title="<?php $category->name(); ?>"><?php $category->name(); ?></a>
                    <svg class="icon" viewBox="0 0 1024 1024" xmlns="http://www.w3.org/2000/svg" width="13" height="13">
                      <path d="M624.865 512.247L332.71 220.088c-12.28-12.27-12.28-32.186 0-44.457 12.27-12.28 32.186-12.28 44.457 0l314.388 314.388c12.28 12.27 12.28 32.186 0 44.457L377.167 848.863c-6.136 6.14-14.183 9.211-22.228 9.211s-16.092-3.071-22.228-9.211c-12.28-12.27-12.28-32.186 0-44.457l292.155-292.16z" />
                    </svg>
                  </div>
                  <ul class="slides panel-body">
                    <?php foreach ($children as $mid) : ?>
                      <?php $child = $category->getCategory($mid); ?>
                      <li>
                        <a class="link <?php echo $this->is('category', $child['slug']) ? 'current' : '' ?>" href="<?php echo $child['permalink'] ?>" title="<?php echo $child['name']; ?>"><?php echo $child['name']; ?></a>
                      </li>
                    <?php endforeach; ?>
                  </ul>
                </li>
              <?php endif; ?>
            <?php endif; ?>
          <?php endwhile; ?>
        </ul>
      </li>
      <!-- 页面 -->
      <li>
        <a class="link panel" href="#" rel="nofollow">
          <span><i class="fa fa-sticky-note"></i>页面</span>
          <svg class="icon" viewBox="0 0 1024 1024" xmlns="http://www.w3.org/2000/svg" width="13" height="13">
            <path d="M624.865 512.247L332.71 220.088c-12.28-12.27-12.28-32.186 0-44.457 12.27-12.28 32.186-12.28 44.457 0l314.388 314.388c12.28 12.27 12.28 32.186 0 44.457L377.167 848.863c-6.136 6.14-14.183 9.211-22.228 9.211s-16.092-3.071-22.228-9.211c-12.28-12.27-12.28-32.186 0-44.457l292.155-292.16z" />
          </svg>
        </a>
        <ul class="slides panel-body">
          <?php foreach ($pages->stack as $item) : ?>
            <li>
              <a class="link <?php echo $this->is('page', $item['slug']) ? 'current' : '' ?>" href="<?php echo $item['permalink'] ?>" title="<?php echo $item['title'] ?>"><?php echo $item['title'] ?></a>
            </li>
          <?php endforeach; ?>
        </ul>
      </li>
      <!-- 推荐 -->
      <?php if (sizeof($custom) > 0) : ?>
        <li>
          <a class="link panel" href="#" rel="nofollow">
            <span><i class="fa fa-send"></i>推荐</span>
            <svg class="icon" viewBox="0 0 1024 1024" xmlns="http://www.w3.org/2000/svg" width="13" height="13">
              <path d="M624.865 512.247L332.71 220.088c-12.28-12.27-12.28-32.186 0-44.457 12.27-12.28 32.186-12.28 44.457 0l314.388 314.388c12.28 12.27 12.28 32.186 0 44.457L377.167 848.863c-6.136 6.14-14.183 9.211-22.228 9.211s-16.092-3.071-22.228-9.211c-12.28-12.27-12.28-32.186 0-44.457l292.155-292.16z" />
            </svg>
          </a>
          <ul class="slides panel-body">
            <?php foreach ($custom as $item) : ?>
              <li>
                <a class="link" href="<?php echo $item['url'] ?>" target="_blank" rel="noopener noreferrer nofollow"><?php echo $item['title'] ?></a>
              </li>
            <?php endforeach; ?>
          </ul>
        </li>
      <?php endif; ?>
    </ul>
    <ul class="joe_header__slideout-menu panel-box" style="margin-top:15px; color:var(--minor);">
        <i class="fa fa-user"></i>
        <?php if($this->user->hasLogin()):?>
        <a class="item" style="color:var(--theme);" href="<?php $this->options->adminUrl(); ?>">
        <?php $this->user->screenName(); ?></a><span>丨</span>
        <a class="item" style="color:var(--minor);" href="<?php $this->options->logoutUrl(); ?>">退出登录</a>
        <?php else : ?>
        <a class="item" style="color:var(--minor);" href=<?php $this->options->adminUrl(); ?> title="注册/登录">注册丨登录</a>
        <?php endif;?>
    </ul>
  </div>

  <div class="joe_header__mask"></div>
</header>
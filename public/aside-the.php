<aside class="joe_aside-the">
  <section class="joe_aside__item author">
    <img width="100%" height="120" class="image lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php $this->options->JAside_Author_Image() ?>" alt="博主栏壁纸" />
    <div class="user">
      <img width="75" height="75" class="avatar lazyload" src="<?php _getAvatarLazyload(); ?>" data-src="<?php $this->options->JAside_Author_Avatar ? $this->options->JAside_Author_Avatar() : _getAvatarByMail($this->authorId ? $this->author->mail : $this->user->mail) ?>" alt="博主头像" />
      <a class="link" href="<?php $this->options->JAside_Author_Link() ?>" target="_blank" rel="noopener noreferrer nofollow"><?php $this->options->JAside_Author_Nick ? $this->options->JAside_Author_Nick() : ($this->authorId ? $this->author->screenName() : $this->user->screenName()); ?></a>
      <p class="motto joe_motto"></p>
    </div>
    <?php Typecho_Widget::widget('Widget_Stat')->to($item); ?>
    <div class="count">
      <div class="item" title="累计文章数">
        <span class="num"><?php echo number_format($item->publishedPostsNum); ?></span>
        <span>文章数</span>
      </div>
      <div class="item" title="累计评论数">
        <span class="num"><?php echo number_format($item->publishedCommentsNum); ?></span>
        <span>评论数</span>
      </div>
      <div class="item" title="累计分类数">
        <span class="num"><?php echo number_format($item->categoriesNum); ?></span>
        <span>分类数</span>
      </div>
      <div class="item" title="累计页面数">
        <span class="num"><?php echo number_format($item->publishedPagesNum + $item->publishedPostsNum); ?></span>
        <span>页面数</span>
      </div>
    </div>
  </section>
  
  <section class="joe_aside__item timelife">
    <div class="joe_aside__item-title">
        <svg class="icon" viewBox="0 0 1024 1024" xmlns="http://www.w3.org/2000/svg" width="18" height="18">
          <path d="M512 938.667A426.667 426.667 0 0 1 85.333 512a421.12 421.12 0 0 1 131.2-306.133 58.88 58.88 0 0 1 42.667-16.64c33.28 1.066 58.027 28.16 84.267 56.96 7.893 8.533 19.626 21.333 28.373 29.013a542.933 542.933 0 0 0 24.533-61.867c18.134-52.266 35.414-101.76 75.307-121.6 55.04-27.733 111.573 37.974 183.253 121.6 16.214 18.774 38.614 44.8 53.547 59.52 1.707-4.48 3.2-8.96 4.48-12.373 8.533-24.32 18.987-54.613 51.2-61.653a57.813 57.813 0 0 1 55.68 20.053A426.667 426.667 0 0 1 512 938.667zM260.693 282.453A336.64 336.64 0 0 0 170.667 512a341.333 341.333 0 1 0 614.826-203.733 90.24 90.24 0 0 1-42.666 50.56 68.267 68.267 0 0 1-53.547 1.706c-25.6-9.173-51.627-38.4-99.2-93.226a826.667 826.667 0 0 0-87.253-91.734 507.733 507.733 0 0 0-26.24 64c-18.134 52.267-35.414 101.76-75.947 119.254-48.853 21.333-88.32-21.334-120.107-56.96-5.76-4.694-13.226-13.014-19.84-19.414z" />
          <path d="M512 810.667A298.667 298.667 0 0 1 213.333 512a42.667 42.667 0 0 1 85.334 0A213.333 213.333 0 0 0 512 725.333a42.667 42.667 0 0 1 0 85.334z" />
        </svg>
        <span class="text">统计</span>
        <span class="title-right_icon"></span>
    </div>
    <div class="joe_aside-the__statistics">
    <?php Typecho_Widget::widget( 'Widget_Stat')->to($stat); ?>
    <div style="">文章总数：<?php $stat->publishedPostsNum() ?></div>
    <div style="">分类总数：<?php $stat->categoriesNum() ?></div>
    <div style="">评论总数：<?php $stat->publishedCommentsNum() ?></div>
    <div style="">页面总数：<?php $stat->publishedPagesNum() ?></div>
    <div style="">主题版本：joe <?php echo _getVersion() ?></div>
    <div style="">typecho版本：typecho <?php $this->options->Version(); ?></div>
    <div style="">页面加载耗时:<?php _endCountTime(); ?>（不准确）</div>
    </div>
  </section>
</aside>
